# Explanation of channels and ApplicationCable available here:
# http://guides.rubyonrails.org/action_cable_overview.html
module ApplicationCable
  class Channel < ActionCable::Channel::Base
  end
end
