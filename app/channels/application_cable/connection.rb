# Explanation of connections and ApplicationCable available here:
# http://guides.rubyonrails.org/action_cable_overview.html
module ApplicationCable
  # The connection class is used to authorize user transactions.
  class Connection < ActionCable::Connection::Base
    # This flag is used to specify the variable identifying the connection to
    # listen on; here we're saying the connection is identified by the logged-in
    # user.
    identified_by :current_user

    # Connect to a client
    def connect
      self.current_user = find_verified_user
    end

    private

    # Find the current user by the current cookie session
    def find_verified_user
      if current_user = User.find_by(id: cookies.signed[:user_id])
        current_user
      else
        reject_unauthorized_connection
      end
    end
  end
end
