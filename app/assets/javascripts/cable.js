//= require action_cable
//= require_self
//= require_tree ./channels

// Create consumer to connect to rails connection cable
(function () {
    this.App || (this.App = {})

    App.cable = ActionCable.createConsumer()
}).call(this)
