# Controller for rendering arbitrary assets
class AssetsController < ApplicationController
  def show
    skip_authorization

    asset_name = params[:asset]

    # render asset_name
    render json: { message: "Hello, World! #{asset_name}" }
    # render json: { message: "Hello, World! #{params}" }
  end
end
